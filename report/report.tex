\documentclass{article}

\usepackage{graphicx}
\graphicspath{ {./assets/images/} }

\title{Ridge regression}
\date{2023-10-01}
\author{Simone Guzzi}

\begin{document}

\maketitle
\pagebreak

\tableofcontents
\pagebreak

\textit{I declare that this material, which I now submit for assessment, is
entirely my own work and has not been taken from the work of others, save and to
the extent that such work has been cited and acknowledged within the text of my
work. I understand that plagiarism, collusion, and copying are grave and serious
offences in the university and accept the penalties that would be imposed should
I engage in plagiarism, collusion or copying. This assignment, or any part of
it, has not been previously submitted by me or any other person for assessment
on this or any other course of study.}

\section{Technologies}

This project is written entirely in Rust. This project has close to zero
dependencies. Among the few dependencies it's worth noting:

\begin{description}
    \item[clap] a command line argument parser that provides a nice CLI
    experience out of the box.
    \item[csv] a csv reader and writer.
    \item[rand] a collection of utilities for random number generation and some
    randomness-related algorithms.
    \item[serde] a framework for serializing and deserializing data structures.
    \item[nalgebra] a real-time computer graphics and physics library with
    support for general-purpose linear algebra.
\end{description}

Rust is a compiled language. The source code is compiled into a binary which is
then natively run on the machine. Binaries for different operating systems are
built using GitLab CI/CD.

\section{Requirements}

Given that the tool is a binary, this project has no requirements.

The one thing the binary expects is to find a \verb|data| directory containing
the datasets and for those datasets to be correctly named.

\begin{verbatim}    
    .
    |- ridge_regression-binary
    |- data
       |- albalone.csv
       |- spotify-tracks-dataset.csv
\end{verbatim}

If you cloned this repository, note that running the binary from the root of
the repository fulfills this requirement.

\section{Installation}

As mentioned, the Rust code is compiled into binaries, which means that no
installation is needed. The binaries can be obtained by either:

\begin{enumerate}
    \item Downloading the binary from the latest release in the project
    repository.
    \item Manually compiling the binary from the source code. If you so choose,
    you'll need to set up a Rust development environment. Instructions on how to
    do so are at https://www.rust-lang.org/learn/get-started.
\end{enumerate}

\section{Usage}

To make user interaction smoother, the project is designed as a command line
tool. To use the tool, just run the binary from a terminal. To display
information about the available subcommands and options, run
\verb|ridge_regression --help| or \verb|ridge_regression <COMMAND> --help|

\begin{verbatim}
ridge_regression --help
    Usage: ridge_regression <COMMAND>
    
    Commands:
      estimate  Estimate the risk for ridge regression given
                alpha and a dataset
      learn     Run ridge regression given alpha and a dataset
      tune      Estimate the risk for ridge regression given a
                set of hyperparameters and a dataset
      help      Print this message or the help of the given
                subcommand(s)
\end{verbatim}

\begin{verbatim}
ridge_regression learn --help
    Run ridge regression given alpha and a dataset
    
    Usage: ridge_regression learn [OPTIONS] <DATASET>
    
    Arguments:
    <DATASET>  [possible values: albalone, spotify]
    
    Options:
    --features <FEATURES>  Which group of features to import
                           [default: all] [possible values:
                           all, categorical, numerical,
                           selected]
    --alpha <ALPHA>        Value for the hyperparameter
                           alpha [default: 0]
\end{verbatim}

\section{Notable implementations}

This project implements the following algorithms and functions:

\begin{description}
    \item[Ridge regression] used for learning predictors for the dataset. It's
    available both as a standalone learning algorithm, given a specific value
    for $\alpha$, and as a family of algorithms, which allows to delegate the
    choice of $\alpha$ to another part of the code - i.e. when using it inside
    nested cross validation. The code simply implements the ridge regression
    formula, using the \verb|nalgebra| library to perform linear algebra. 
    \item[K-fold cross validation] used for estimating the risk of ridge
    regression over the dataset. Before partitioning the dataset into train sets
    and test sets, the code shuffles the dataset to get around proximity of
    similar data points in the csv file.
    \item[Nested cross validation] used for hyperparameter tuning. Similarly to
    the k-fold cross validation algorithm, the algorithm shuffles the dataset
    before partitioning it.
    \item[Quadratic Loss] used as loss function for all cross validation
    algorithms. 
\end{description}

The code for all these algorithms is in \verb|src/learn.rs|. Moreover this
project also implements:

\begin{description}
    \item[Target encoder] used to encode categorical features.
    \item[Clipper] used to normalize numerical features that have very extreme
    outliers. This function constrains values between the first and the third
    quartile of the numerical feature.
    \item[Range scaler] used to scale numerical features that have a much wider
    range of values compared to the rest. This function constrains values in
    $[0,1]$.
\end{description}

The code for all these functions is in \verb|src/datasets.rs|. One last notable
implementation is a partition helper, used to partition a dataset into
$S_i$/$S_{-i}$ pairs for cross validation. This function is in
\verb|src/utils.rs|.

\section{Dataset}

This project uses two datasets:

\begin{enumerate}
    \item The Spotify tracks dataset. This is the focus of the experiment.
    \item The Albalone dataset. This is a reference dataset for regression and
    it can be used as reference when implementing the core algorithms of the
    tool.
\end{enumerate}

Using a reference dataset of well known properties is extremely valuable during
the implementation phase. That said, for the sake of conciseness, the content of
this report focuses solely on the Spotify dataset, as the Albalone dataset is
outside of the scope of the experiment.

The datasets are read using the \verb|csv| library and each line is deserialized
into a Rust object using \verb|serde|. To do so, \verb|serde| relies on a
definition of the object to extract, both in terms of which columns to lookup
and which type to use in parsing the information contained in those columns.
Such definitions are in \verb|src/models.rs|.

\subsection{Features}

As far as this experiment is concerned, the Spotify dataset contains 18 features
and one label. The label to be predicted is the track's popularity.

The code that extracts the features is in \verb|src/datasets.rs|.

\subsubsection{Numerical features}

The following features are semantically significant as numbers, which is why
they are treated as such:

\begin{description}
    \item[duration\_ms] clipped at the first and third quartile and scaled to
    $[0,1]$.
    \item[danceability]
    \item[energy]
    \item[loudness] clipped at the first and third quartile and scaled to
    $[0,1]$.
    \item[mode]
    \item[speechiness]
    \item[acousticness]
    \item[instrumentalness]
    \item[liveness]
    \item[valence]
    \item[tempo] clipped at the first and third quartile and scaled to $[0,1]$.
\end{description}

To make the predictor more stable, clipping and range scaling are used to
normalize numerical features that show highly uneven distribution of values. A
good example of such a feature is the duration - Figure \ref{duration-box-plot}
-, which in its raw form is considerably out of scale with respect to other
features - Figure \ref{danceability-box-plot} - and has very extreme outliers.

\begin{figure}[ht]
    \includegraphics[width=\textwidth]{duration-box-plot.png}
    \caption{Box-plot for track's duration before feature normalization.}
    \label{duration-box-plot}
\end{figure}

\begin{figure}[ht]
    \includegraphics[width=\textwidth]{danceability-box-plot.png}
    \caption{Box-plot for track's danceability.}
    \label{danceability-box-plot}
\end{figure}

\subsubsection{Categorical features}

The following features are considered categorical features:

\begin{description}
    \item[artists] encoded using target encoding.
    \item[album\_name] encoded using target encoding.
    \item[track\_name] encoded using target encoding.
    \item[explicit] encoded using target encoding.
    \item[key] encoded using target encoding. The value is actually represented
    as a number, but it makes semantically more sense if treated as a category
    as there is no concept of ordering between keys.
    \item[time\_signature] encoded using target encoding. The value is actually
    represented as a number, but it makes semantically more sense if treated as
    a category as there is no concept of ordering between time signatures.
    \item[track\_genre] encoded using target encoding.
\end{description}

Target encoding is used in order to control curse of dimensionality as some of
these features have a very high number of categories.

\subsection{Assumptions and approximations}

This experiment assumes full correctness and completeness of the datasets. This
is because Rust has high demands in terms of error handling and tackling errors
generated by an imperfect dataset would make the code more complex and verbose
than it already is. This assumption is made on the basis of code quality not
being the primary focus of this experiment and considering the academic nature
of the datasets fed to the algorithms. In practice, primitives such as
\verb|unwrap| are highly discouraged and would be replaced by proper error
handling.

When a track has multiple artists, this experiment approximates a track's
artists to the semicolon separated list of artists provided by the dataset
itself. This approach has the advantage of being very simple to implement. It
also has the drawback of introducing a lot categories for this feature. Some
alternatives are:

\begin{itemize}
    \item Approximating a track's artists with the first item of the semicolon
    separated list.
    \item Splitting the semicolon separated list of artists to isolate each
    artist and unwind the track into multiple tracks, one per artist obtained in
    this way.
\end{itemize}

\section{Experiment}

\subsection{Choosing the hyperparameter}

The \verb|tune flat| subcommand allows to experiment with different values for
$\alpha$. Given that the set of all possible values of $\alpha$ is infinite, we
need to run the experiment over a finite and reasonably small subset $\Theta_0$.
Given a list of values for $\alpha$ - submitted using the \verb|--theta-set|
option -, the program runs k-fold cross validation iterating over those values.
It's also possible to let the program generate those values automatically, while
optionally putting some constraints to such generation. For details about those
constraints, run \verb|ridge-regression tune flat --help|. One can then compare
risk estimates obtained with different parameters and identify the value of
$\alpha$ that performs best. Note that this approach is known to slightly
underestimate the risk.

To choose the best performing value for $\alpha$, the \verb|tune flat|
subcommand is run repeatedly and $\Theta_0$ is adjusted according to the
result in the following manner:

\begin{enumerate}
    \item If in the current iteration $i$ the best risk estimate is obtained for
    the highest value of $\Theta_i$, in the next iteration $j$ choose a higher
    bound for $\Theta_j$ which is higher than the one chosen for $\Theta_i$.
    \item If in the current iteration $i$ the best risk estimate is obtained for
    the lowest value of $\Theta_i$, in the next iteration $j$ choose a lower
    bound for $\Theta_j$ which is lower than the one chosen for $\Theta_i$.
    \item If in the current iteration $i$ the best risk estimate is obtained for
    an internal value of $\Theta_i$, in the next iteration $j$ choose its
    neighbors as lower and higher bound for $\Theta_j$.
\end{enumerate}

Note that this procedure may get stuck into local minima.

For the Spotify dataset, the best risk estimate is obtained when
$\alpha\approx1250$.

A better estimate can be obtained by using the \verb|tune nested| subcommand.
This subcommand runs k-fold nested cross validation. Due to how the algorithm is
implemented and the output of the program while running it, it's harder for the
user to identify the value of $\alpha$ that performs best.

\subsection{Numerical features only}

Running ridge regression on the Spotify dataset with numerical features only
shows very bad results, regardless of the value chosen for $\alpha$.

\begin{verbatim}
    ridge_regression estimate spotify --features numerical
    --alpha 1250
    
    [...]
    
    The risk estimate obtained by using 5-fold cross
    validation is 492.12749
\end{verbatim}

This can be due to different reasons:

\begin{description}
    \item[Bad normalization] Some of the numerical features present uneven
    distributions of values. The normalization performed on the dataset is very
    lightweight and could be improved with a deeper analysis on each single
    feature.
    \item[Bad feature selection] All of the numerical features of the dataset
    are selected as part of the feature space. Some of those features might have
    very low correlation with a track's popularity and they might harm the
    result as a consequence.
    \item[Irrelevant features] It's possible that all of the numerical features
    contained in this dataset are irrelevant - or only partly relevant - for
    predicting a track's popularity.
\end{description}

Due to the extraordinarily high risk estimate produced by cross-validation,
fallacy in the implementation could be considered as one of the reasons for such
a bad result. This reason can be ruled out thanks to the reference set. Running
the same code on the Albalone dataset for numerical features only leads to much
better results:

\begin{verbatim}
    ridge_regression estimate albalone --features numerical
    --alpha 1

    [...]

    The risk estimate obtained by using 5-fold cross
    validation is 5.11199
\end{verbatim}

\subsection{Numerical and categorical features}

Results improve drastically when including categorical features as well.

\begin{verbatim}
    ridge_regression estimate spotify --features all
    --alpha 1250

    [...]

    The risk estimate obtained by using 5-fold cross
    validation is 13.15601
\end{verbatim}

A track's album in particular has a huge impact on the risk estimate, to the
point that running ridge regression on that feature alone leads to a result
which is suspiciously close to the result obtained by including all features.

\begin{verbatim}
    ridge_regression estimate spotify --features selected
    --alpha 1250

    [...]

    The risk estimate obtained by using 5-fold cross
    validation is 14.93882
\end{verbatim}

This can be due to different reasons:

\begin{description}
    \item[Overfitting] Target encoding is known to introduce overfitting when
    used on categories that appear only a few times in the dataset. A track's
    album certainly shows this property. The solution is introducing a smoothing
    factor when performing the encoding.
    \item[High correlation] It's possible that a track's popularity is mostly
    dependent on the album in which it was published.
\end{description}

\subsection{Findings}

In conclusion, predicting Spotify tracks popularity seems to be mostly about
their album. To rule out overfitting and confirm this theory, one should
introduce a smoothing factor when performing target encoding, and tune that
parameter as well. Moreover, numerical features - or at least the ones that are
part of this dataset - don't seem to play enough of a role when it comes to
predicting a track's popularity.

\section{Future improvements}

\begin{description}
    \item[Normalize numerical features] Right now most numerical features are
    used as they are, without any normalization. Some features might benefit
    from normalization. A deeper analysis should be carried out and numerical
    features normalized accordingly. 
    \item[Optimize categorical feature encoding] Right now each categorical
    feature is encoded using target encoding without any smoothing factor. Some
    features might benefit by using a smoothing factor - i.e. A track's album is
    likely to lead to overfitting if encoded without a smoothing factor - or by
    using different types of encoding - i.e. A track's key could be easily
    represented by using one-hot encoding. A deeper analysis should be carried
    out and categorical encodings adjusted accordingly.
    \item[Perform feature selection] Right now all available features are
    selected. Some features might be harming the result rather than improving
    it. A deeper analysis should be carried out and only a subset of the
    available features should be selected accordingly.
    \item[Run algorithms in parallel] Right now all parts of the computation are
    performed serially. Algorithms like cross validation and nested cross
    validation are perfect candidates for parallel computing. The program should
    take advantage of Rust support for multi threading and run those algorithms
    in parallel to be more performant. 
    \item[Profile] Right now there is no information about execution time.
    Performance is a key metric of a learning algorithm and its analysis should
    be supported. The program should keep track of execution time and include it
    in the output.
    \item[Point at tuned value for $\alpha$] The \verb|tune| subcommand iterates
    through different values for $\alpha$. By reading the output one could
    identify the value of $\alpha$ which performs better for the training set.
    This is error-prone and annoying. The program should just explicitly point
    to such tuned value at the end of execution.
    \item[Improve output readability] The program output is extremely verbose
    and cumbersome to read. The program should use a proper logging system.
    \item[Datasets location] The program expects to find the csv files
    containing the datasets at a specific location. The CLI should offer an
    option for the user to specify the dataset location.
\end{description}

\section{Resources}

\begin{description}
    \item[Kaggle] Source of the spotify tracks dataset. The spotify tracks
    dataset can be found at
    https://www.kaggle.com/datasets/maharshipandya/-spotify-tracks-dataset.
    \item[UCI repository of machine learning databases] Source of the Albalone
    dataset. The Albalone dataset can be found at
    http://archive.ics.uci.edu/dataset/1/abalone.
    \item[sett-rs] Inspiration for part of the implementation. Namely, the CLI
    implementation and GitLab CI/CD jobs for building and publishing binaries
    are taken from the project's open source code and heavily revisited. Note
    that this code is merely infrastructural and outside of the scientific scope
    of this experiment. The project's repository can be found at
    https://gitlab.com/biomedit/sett-rs.
\end{description}

\end{document}
