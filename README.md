# Ridge regression

This repository hosts my submission for the experimental project of the
2022/2023 edition of the course in statistical methods for machine learning.

Please find:

- The binaries for the `ridge_regression` tool as well as the pdf version of the
  report attached to the latest entry of the [releases
  page](https://gitlab.com/uni88/ridge-regression/-/releases).
- The source code for the `ridge_regression` tool in the `src` folder.
- The source code for the pdf report in the `report` folder.
- The datasets used to develop the tool in the `data` folder. Note that you will
  need to have those datasets locally on your machine in order to correctly run
  the tool.

Requirements and instructions about downloading and using the tool are in the
pdf report.
