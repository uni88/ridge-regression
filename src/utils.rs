fn skip_nth<T: Clone>(chunks: &[Vec<T>], n: usize) -> Vec<T> {
    let first_half: Vec<T> = chunks
        .iter()
        .take(n)
        .map(|chunk| chunk.to_vec())
        .reduce(|acc, other| [acc, other].concat())
        .unwrap_or_default();
    let second_half: Vec<T> = chunks
        .iter()
        .skip(n + 1)
        .map(|chunk| chunk.to_vec())
        .reduce(|acc, other| [acc, other].concat())
        .unwrap_or_default();
    [first_half, second_half].concat()
}

fn take_nth<T: Clone>(chunks: &[Vec<T>], n: usize) -> Vec<T> {
    chunks
        .get(n)
        .map(|chunk| chunk.to_vec())
        .unwrap_or_default()
}

pub fn partition<T: Clone>(dataset: &Vec<T>, n: usize) -> impl Iterator<Item = (Vec<T>, Vec<T>)> {
    let chunk_size = dataset.len() / n;

    let x_chunks: Vec<Vec<T>> = dataset
        .chunks(chunk_size)
        .map(|chunk| chunk.to_vec())
        .collect();

    (0..n).map(move |i| (skip_nth(&x_chunks, i), take_nth(&x_chunks, i)))
}

pub fn space_equally(min: f64, max: f64, steps: usize) -> Vec<f64> {
    let step = (max - min) / ((steps - 1) as f64);
    (0..steps).map(|i| min + (i as f64 * step)).collect()
}
