use std::iter::zip;

use clap::ValueEnum;

use crate::models::{Albalone, Track};
use std::{collections::HashMap, hash::Hash};

pub enum Features {
    All,
    Categorical,
    Numerical,
    Selected,
}

#[derive(ValueEnum, Debug, Clone)]
pub enum FeaturesArg {
    All,
    Categorical,
    Numerical,
    Selected,
}

impl From<FeaturesArg> for Features {
    fn from(arg: FeaturesArg) -> Self {
        match arg {
            FeaturesArg::All => Features::All,
            FeaturesArg::Categorical => Features::Categorical,
            FeaturesArg::Numerical => Features::Numerical,
            FeaturesArg::Selected => Features::Selected,
        }
    }
}

pub trait Dataset {
    fn numerical_features(&self) -> Vec<Vec<f64>>;
    fn categorical_features(&self) -> Vec<Vec<f64>>;
    fn selected_features(&self) -> Vec<Vec<f64>>;
    fn data_points(&self, features: &Features) -> Vec<Vec<f64>> {
        match features {
            Features::All => zip(self.numerical_features(), self.categorical_features())
                .map(|(a, b)| [a, b].concat())
                .collect(),
            Features::Categorical => self.categorical_features(),
            Features::Numerical => self.numerical_features(),
            Features::Selected => self.selected_features(),
        }
    }
    fn labels(&self) -> Vec<f64>;
}

fn target_encoder<T>(categorical_feature: &[T], target_variable: &Vec<f64>) -> impl Fn(T) -> f64
where
    T: Eq + PartialEq + Hash + Clone,
{
    let mut category_counts: HashMap<T, usize> = HashMap::new();
    let mut category_means: HashMap<T, f64> = HashMap::new();

    for (category, target) in categorical_feature.iter().zip(target_variable.iter()) {
        let count = category_counts.entry(category.clone()).or_insert(0);
        *count += 1;
        let sum = category_means.entry(category.clone()).or_insert(0.);
        *sum += target;
    }

    for category in category_counts.clone().keys() {
        let mean = category_means.entry(category.clone()).or_default();
        let count = category_counts.entry(category.clone()).or_default();
        *mean /= *count as f64;
    }

    let mean = target_variable.iter().sum::<f64>() / (target_variable.len() as f64);
    move |feature| *category_means.get(&feature).unwrap_or(&mean)
}

fn clipper(numerical_feature: &[f64]) -> impl Fn(f64) -> f64 {
    let n = numerical_feature.len();
    let first_quartile_index = (n as f64 * 0.25) as usize;
    let third_quartile_index = (n as f64 * 0.75) as usize;
    let mut sorted_feature = numerical_feature.to_vec();
    sorted_feature.sort_by(f64::total_cmp);
    let first_quartile = sorted_feature[first_quartile_index];
    let third_quartile = sorted_feature[third_quartile_index];

    move |feature| f64::max(first_quartile, f64::min(feature, third_quartile))
}

fn range_scaler(numerical_feature: &[f64]) -> impl Fn(f64) -> f64 {
    let max = numerical_feature
        .iter()
        .copied()
        .max_by(f64::total_cmp)
        .unwrap_or_default();
    let min = numerical_feature
        .iter()
        .copied()
        .min_by(f64::total_cmp)
        .unwrap_or_default();

    move |feature| (feature - min) / (max - min)
}

impl Dataset for Vec<Track> {
    fn numerical_features(&self) -> Vec<Vec<f64>> {
        let duration_clipper = clipper(
            &self
                .iter()
                .map(|track| track.duration_ms)
                .collect::<Vec<f64>>(),
        );
        let duration_scaler = range_scaler(
            &self
                .iter()
                .map(|track| track.duration_ms)
                .collect::<Vec<f64>>(),
        );
        let loudness_clipper = clipper(
            &self
                .iter()
                .map(|track| track.loudness)
                .collect::<Vec<f64>>(),
        );
        let loudness_scaler = range_scaler(
            &self
                .iter()
                .map(|track| track.loudness)
                .collect::<Vec<f64>>(),
        );
        let tempo_clipper = clipper(&self.iter().map(|track| track.tempo).collect::<Vec<f64>>());
        let tempo_scaler =
            range_scaler(&self.iter().map(|track| track.tempo).collect::<Vec<f64>>());
        self.iter()
            .map(|track| {
                vec![
                    duration_scaler(duration_clipper(track.duration_ms)),
                    track.danceability,
                    track.energy,
                    loudness_scaler(loudness_clipper(track.loudness)),
                    track.mode,
                    track.speechiness,
                    track.acousticness,
                    track.instrumentalness,
                    track.liveness,
                    track.valence,
                    tempo_scaler(tempo_clipper(track.tempo)),
                ]
            })
            .collect()
    }
    fn categorical_features(&self) -> Vec<Vec<f64>> {
        let explicit_encoder = target_encoder(
            &self
                .iter()
                .map(|track| &track.explicit)
                .collect::<Vec<&String>>(),
            &self.labels(),
        );
        let genre_encoder = target_encoder(
            &self
                .iter()
                .map(|track| &track.track_genre)
                .collect::<Vec<&String>>(),
            &self.labels(),
        );
        let artists_encoder = target_encoder(
            &self
                .iter()
                .map(|track| &track.artists)
                .collect::<Vec<&String>>(),
            &self.labels(),
        );
        let album_encoder = target_encoder(
            &self
                .iter()
                .map(|track| &track.album_name)
                .collect::<Vec<&String>>(),
            &self.labels(),
        );
        let track_name_encoder = target_encoder(
            &self
                .iter()
                .map(|track| &track.track_name)
                .collect::<Vec<&String>>(),
            &self.labels(),
        );
        let key_encoder = target_encoder(
            &self.iter().map(|track| &track.key).collect::<Vec<&usize>>(),
            &self.labels(),
        );
        let time_signature_encoder = target_encoder(
            &self
                .iter()
                .map(|track| &track.time_signature)
                .collect::<Vec<&usize>>(),
            &self.labels(),
        );
        self.iter()
            .map(|track| {
                vec![
                    artists_encoder(&track.artists),
                    album_encoder(&track.album_name),
                    track_name_encoder(&track.track_name),
                    explicit_encoder(&track.explicit),
                    key_encoder(&track.key),
                    time_signature_encoder(&track.key),
                    genre_encoder(&track.track_genre),
                ]
            })
            .collect()
    }
    fn selected_features(&self) -> Vec<Vec<f64>> {
        let album_encoder = target_encoder(
            &self
                .iter()
                .map(|track| &track.album_name)
                .collect::<Vec<&String>>(),
            &self.labels(),
        );
        self.iter()
            .map(|track| vec![album_encoder(&track.album_name)])
            .collect()
    }
    fn labels(&self) -> Vec<f64> {
        self.iter().map(|track| track.popularity).collect()
    }
}

impl Dataset for Vec<Albalone> {
    fn numerical_features(&self) -> Vec<Vec<f64>> {
        self.iter()
            .map(|albalone| {
                vec![
                    albalone.length,
                    albalone.diameter,
                    albalone.height,
                    albalone.whole_weight,
                    albalone.shucked_weight,
                    albalone.viscera_weight,
                    albalone.shell_weight,
                ]
            })
            .collect()
    }
    fn categorical_features(&self) -> Vec<Vec<f64>> {
        let sex_encoder = target_encoder(
            &self
                .iter()
                .map(|albalone| albalone.sex)
                .collect::<Vec<char>>(),
            &self.labels(),
        );
        self.iter()
            .map(|albalone| vec![sex_encoder(albalone.sex)])
            .collect()
    }
    fn labels(&self) -> Vec<f64> {
        self.iter().map(|albalone| albalone.rings).collect()
    }
    fn selected_features(&self) -> Vec<Vec<f64>> {
        self.data_points(&Features::All)
    }
}
