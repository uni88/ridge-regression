use nalgebra::DMatrix;
use std::iter::zip;

use rand::seq::SliceRandom;
use rand::thread_rng;

use crate::{
    datasets::{Dataset, Features},
    utils::partition,
};

type LearningAlgorithm = dyn Fn(&Vec<Vec<f64>>, &Vec<f64>) -> Box<Predictor>;
type LearningAlgorithmFamily = dyn Fn(f64) -> Box<LearningAlgorithm>;
type Predictor = dyn Fn(&Vec<f64>) -> f64;
type LossFunction = dyn Fn(f64, f64) -> f64;

pub fn ridge_regression_with_alpha(s: &Vec<Vec<f64>>, y: &Vec<f64>, alpha: f64) -> Box<Predictor> {
    println!(
        "              📐 Running ridge regression with alpha = {}",
        alpha
    );

    let s = &DMatrix::from_row_slice(s.len(), s[0].len(), &s.concat());
    let y = &DMatrix::from_row_slice(y.len(), 1, y);
    let i = DMatrix::identity(s.ncols(), s.ncols());
    let inverse = (alpha * i + s.transpose() * s).try_inverse().unwrap();
    let w = inverse * s.transpose() * y;

    println!(
        "              📐🔮 The vector obtained with alpha = {} is {}",
        alpha, w
    );

    Box::new(move |x| (DMatrix::from_row_slice(1, x.len(), x) * w.clone()).norm())
}

pub fn ridge_regression(alpha: f64) -> Box<LearningAlgorithm> {
    Box::new(move |s, y| ridge_regression_with_alpha(s, y, alpha))
}

pub fn quadratic_loss(y: f64, h_x: f64) -> f64 {
    (y - h_x).powf(2.)
}

pub fn k_fold_cross_validation<T: Clone>(
    dataset: &[T],
    features: &Features,
    a: &LearningAlgorithm,
    l: &LossFunction,
    k: usize,
) -> f64
where
    Vec<T>: Dataset,
{
    println!("      🔎 Running {}-fold cross validation", k);
    let mut i = 1;
    let mut rng = thread_rng();
    let mut local_dataset = dataset.to_vec();
    local_dataset.shuffle(&mut rng);

    let test_errors = partition(&local_dataset, k).map(|(train, test)| {
        let n = test.len();
        let h_i = &a(&train.data_points(features), &train.labels());
        let test_error = zip(
            test.labels(),
            test.data_points(features)
                .iter()
                .map(h_i)
                .collect::<Vec<f64>>(),
        )
        .map(|(y, h_x)| l(y, h_x))
        .sum::<f64>()
            / n as f64;

        println!("      🔎🎯 {}-th test error is {:.5}", i, test_error);
        i += 1;

        test_error
    });

    let risk_estimate = test_errors.sum::<f64>() / k as f64;

    println!(
        "       🔎📈 The risk estimate obtained by using {}-fold cross \
        validation is {:.5}",
        k, risk_estimate
    );

    risk_estimate
}

pub fn k_fold_nested_cross_validation<T: Clone>(
    dataset: &[T],
    features: &Features,
    family: &LearningAlgorithmFamily,
    l: &LossFunction,
    theta_set: &[f64],
    k: usize,
    inner_k: usize,
) -> f64
where
    Vec<T>: Dataset,
{
    println!(
        "🪺  Running {}-fold nested cross validation using {}-fold cross \
        validation over {} parameters",
        k,
        inner_k,
        theta_set.len()
    );
    let mut i = 1;
    let mut rng = thread_rng();
    let mut local_dataset = dataset.to_vec();
    local_dataset.shuffle(&mut rng);

    let test_errors = partition(&local_dataset, k).map(|(train, test)| {
        let theta_i = theta_set
            .iter()
            .map(|theta| {
                let a_theta = family(*theta);

                println!(
                    "🪺  Running {}-fold cross validation with theta = {:.5}",
                    inner_k, theta
                );

                (
                    *theta,
                    k_fold_cross_validation(&train, features, &a_theta, l, inner_k),
                )
            })
            .min_by(|(_, risk_estimate_i), (_, risk_estimate_j)| {
                risk_estimate_i.total_cmp(risk_estimate_j)
            })
            .map(|(theta_i, _)| theta_i)
            .unwrap_or_default();

        println!("🪺 🎚️  {}-th theta is {:.5}", i, theta_i);

        let n = test.len();
        let a_i = family(theta_i);
        let h_i = a_i(&train.data_points(features), &train.labels());
        let test_error = zip(
            test.labels(),
            test.data_points(features)
                .iter()
                .map(h_i)
                .collect::<Vec<f64>>(),
        )
        .map(|(y, h_i_x)| l(y, h_i_x))
        .sum::<f64>()
            / n as f64;

        println!("🪺 🎯 {}-th error is {:.5}", i, test_error);
        i += 1;

        test_error
    });

    let risk_estimate = test_errors.sum::<f64>() / k as f64;

    println!(
        "🪺 📈 The risk estimate obtained by using {}-fold nested cross \
        validation using {}-fold cross validation over {} parameters is {:.5}",
        k,
        inner_k,
        theta_set.len(),
        risk_estimate
    );

    risk_estimate
}
