use clap::Args;
use ridge_regression::{
    learn::{k_fold_cross_validation, quadratic_loss, ridge_regression},
    models::{Albalone, Track},
};

use super::{common::DatasetArg, Command};
use anyhow::Result;

#[derive(Args)]
pub(crate) struct Estimate {
    #[command(flatten)]
    dataset: super::common::DatasetArgs,

    #[command(flatten)]
    cross_validation: super::common::CrossValidationArgs,

    #[command(flatten)]
    ridge_regression: super::common::RidgeRegressionArgs,
}

impl Command for Estimate {
    fn run(&self) -> Result<()> {
        match self.dataset.dataset {
            DatasetArg::Spotify => k_fold_cross_validation(
                &self.dataset.get_dataset::<Track>()?,
                &self.dataset.features.clone().into(),
                &ridge_regression(self.ridge_regression.alpha),
                &quadratic_loss,
                self.cross_validation.k,
            ),
            DatasetArg::Albalone => k_fold_cross_validation(
                &self.dataset.get_dataset::<Albalone>()?,
                &self.dataset.features.clone().into(),
                &ridge_regression(self.ridge_regression.alpha),
                &quadratic_loss,
                self.cross_validation.k,
            ),
        };

        Ok(())
    }
}
