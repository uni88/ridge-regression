use anyhow::{anyhow, Result};
use clap::{Args, ValueEnum};
use ridge_regression::datasets::FeaturesArg;

#[derive(ValueEnum, Debug, Clone)]
pub enum DatasetArg {
    Albalone,
    Spotify,
}

#[derive(Args)]
pub(super) struct DatasetArgs {
    pub(super) dataset: DatasetArg,

    /// Which group of features to import
    #[arg(long, value_enum, default_value_t = FeaturesArg::All)]
    pub(super) features: FeaturesArg,
}

impl DatasetArgs {
    pub fn get_dataset<T: for<'de> serde::Deserialize<'de>>(&self) -> Result<Vec<T>> {
        println!(
            "{} Importing dataset",
            match self.dataset {
                DatasetArg::Spotify => "🎹",
                DatasetArg::Albalone => "🐚",
            }
        );

        let file_path = match self.dataset {
            DatasetArg::Spotify => "./data/spotify-tracks-dataset.csv",
            DatasetArg::Albalone => "./data/albalone.csv",
        };

        let dataset = if let Ok(mut reader) = csv::Reader::from_path(file_path) {
            Ok(reader
                .deserialize::<T>()
                .map(|item| item.unwrap())
                .collect::<Vec<T>>())
        } else {
            Err(anyhow!("💥 Error while reading file"))
        }?;

        println!("📥 Imported dataset of size {}", dataset.len(),);

        Ok(dataset)
    }
}

#[derive(Args)]
pub(super) struct CrossValidationArgs {
    /// Number of folds for the cross validation
    #[arg(long, default_value_t = 5)]
    pub(super) k: usize,
}

#[derive(Args)]
pub(super) struct RidgeRegressionArgs {
    /// Value for the hyperparameter alpha
    #[arg(long, default_value_t = 0.)]
    pub alpha: f64,
}
