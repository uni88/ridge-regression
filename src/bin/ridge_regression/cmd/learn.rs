use clap::Args;
use ridge_regression::{
    datasets::Dataset,
    learn::ridge_regression_with_alpha,
    models::{Albalone, Track},
};

use super::{common::DatasetArg, Command};
use anyhow::Result;

#[derive(Args)]
pub(crate) struct Learn {
    #[command(flatten)]
    dataset: super::common::DatasetArgs,

    #[command(flatten)]
    ridge_regression: super::common::RidgeRegressionArgs,
}

impl Command for Learn {
    fn run(&self) -> Result<()> {
        match self.dataset.dataset {
            DatasetArg::Spotify => {
                let dataset = self.dataset.get_dataset::<Track>()?;
                let _ = ridge_regression_with_alpha(
                    &dataset.data_points(&self.dataset.features.clone().into()),
                    &dataset.labels(),
                    self.ridge_regression.alpha,
                );
            }
            DatasetArg::Albalone => {
                let dataset = self.dataset.get_dataset::<Albalone>()?;
                let _ = ridge_regression_with_alpha(
                    &dataset.data_points(&self.dataset.features.clone().into()),
                    &dataset.labels(),
                    self.ridge_regression.alpha,
                );
            }
        }

        Ok(())
    }
}
