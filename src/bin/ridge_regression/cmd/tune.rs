use clap::{Args, Subcommand};
use ridge_regression::{
    learn::{
        k_fold_cross_validation, k_fold_nested_cross_validation, quadratic_loss, ridge_regression,
    },
    models::{Albalone, Track},
    utils::space_equally,
};

use crate::cmd::common::DatasetArg;

use super::Command;
use anyhow::Result;

#[derive(Subcommand)]
pub(crate) enum Tune {
    /// Estimate the risk for ridge regression using flat cross validation
    Flat(Flat),
    /// Estimate the risk for ridge regression using nested cross validation
    Nested(Nested),
}

#[derive(Args)]
pub(crate) struct Theta {
    /// A given set of hyperparameters as a comma separated list of parameters
    #[arg(long, conflicts_with = "theta_set_size", conflicts_with = "min_theta",
conflicts_with = "max_theta", num_args = 1.., value_parser, value_delimiter=',')]
    pub theta_set: Option<Vec<f64>>,

    /// Number of members for the automatically generated hyperparameters set
    #[arg(long, conflicts_with = "theta_set", default_value_t = 5)]
    pub theta_set_size: usize,

    /// Min value for members of the automatically generated hyperparameters set
    #[arg(long, conflicts_with = "theta_set", default_value_t = 0.)]
    pub min_theta: f64,

    /// Max value for members of the automatically generated hyperparameters set
    #[arg(long, conflicts_with = "theta_set", default_value_t = 100.)]
    pub max_theta: f64,
}

impl Theta {
    fn get_theta_set(&self) -> Vec<f64> {
        self.theta_set.clone().unwrap_or(space_equally(
            self.min_theta,
            self.max_theta,
            self.theta_set_size,
        ))
    }
}

#[derive(Args)]
pub(crate) struct TuneArgs {
    #[command(flatten)]
    dataset: super::common::DatasetArgs,

    #[command(flatten)]
    cross_validation: super::common::CrossValidationArgs,

    #[command(flatten)]
    theta: Theta,
}

#[derive(Args)]
pub(crate) struct Flat {
    #[command(flatten)]
    tune: TuneArgs,
}

impl Command for Flat {
    fn run(&self) -> Result<()> {
        let theta_set = self.tune.theta.get_theta_set();

        println!("🧮 Parameters set is {:#?}", theta_set);

        let risk_estimate = theta_set
            .iter()
            .map(|theta| {
                let a_theta = ridge_regression(*theta);

                println!(
                    "🗺️  Running {}-fold cross validation with theta = {:.5}",
                    self.tune.cross_validation.k, theta
                );

                match self.tune.dataset.dataset {
                    DatasetArg::Spotify => k_fold_cross_validation(
                        &self.tune.dataset.get_dataset::<Track>().unwrap(),
                        &self.tune.dataset.features.clone().into(),
                        &a_theta,
                        &quadratic_loss,
                        self.tune.cross_validation.k,
                    ),
                    DatasetArg::Albalone => k_fold_cross_validation(
                        &self.tune.dataset.get_dataset::<Albalone>().unwrap(),
                        &self.tune.dataset.features.clone().into(),
                        &a_theta,
                        &quadratic_loss,
                        self.tune.cross_validation.k,
                    ),
                }
            })
            .min_by(f64::total_cmp)
            .unwrap_or_default();

        println!(
            "🗺️ 📈 The risk estimate obtained by using {}-fold cross \
            validation over {} parameters is {:.5}",
            self.tune.cross_validation.k,
            theta_set.len(),
            risk_estimate
        );

        Ok(())
    }
}

#[derive(Args)]
pub(crate) struct Nested {
    #[command(flatten)]
    tune: TuneArgs,

    /// Number of folds for the outer cross validation
    #[arg(long, default_value_t = 5)]
    pub outer_k: usize,
}

impl Command for Nested {
    fn run(&self) -> Result<()> {
        let theta_set = self.tune.theta.get_theta_set();

        println!("🧮 Parameters set is {:#?}", theta_set);

        match self.tune.dataset.dataset {
            DatasetArg::Spotify => k_fold_nested_cross_validation(
                &self.tune.dataset.get_dataset::<Track>()?,
                &self.tune.dataset.features.clone().into(),
                &ridge_regression,
                &quadratic_loss,
                &theta_set,
                self.outer_k,
                self.tune.cross_validation.k,
            ),
            DatasetArg::Albalone => k_fold_nested_cross_validation(
                &self.tune.dataset.get_dataset::<Albalone>()?,
                &self.tune.dataset.features.clone().into(),
                &ridge_regression,
                &quadratic_loss,
                &theta_set,
                self.outer_k,
                self.tune.cross_validation.k,
            ),
        };

        Ok(())
    }
}

impl crate::cmd::Command for Tune {
    fn run(&self) -> Result<()> {
        match self {
            Self::Flat(c) => c.run(),
            Self::Nested(c) => c.run(),
        }
    }
}
