use anyhow::Result;
use clap::Parser;

use crate::cmd::Command;

mod cmd;

#[derive(Parser)]
#[command()]
enum Base {
    /// Estimate the risk for ridge regression given alpha and a dataset
    Estimate(cmd::estimate::Estimate),
    /// Run ridge regression given alpha and a dataset
    Learn(cmd::learn::Learn),
    /// Estimate the risk for ridge regression given a set of hyperparameters
    /// and a dataset
    #[command(subcommand)]
    Tune(cmd::tune::Tune),
}

impl Command for Base {
    fn run(&self) -> Result<()> {
        match self {
            Self::Estimate(c) => c.run(),
            Self::Learn(c) => c.run(),
            Self::Tune(c) => c.run(),
        }
    }
}

fn main() -> Result<()> {
    Base::parse().run()
}
