use anyhow::Result;

mod common;
pub(super) mod estimate;
pub(super) mod learn;
pub(super) mod tune;

pub(crate) trait Command {
    fn run(&self) -> Result<()>;
}
