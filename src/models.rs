#[derive(Debug, serde::Deserialize, Clone)]
pub struct Track {
    pub artists: String,
    pub album_name: String,
    pub track_name: String,
    pub popularity: f64,
    pub duration_ms: f64,
    pub explicit: String,
    pub danceability: f64,
    pub energy: f64,
    pub key: usize,
    pub loudness: f64,
    pub mode: f64,
    pub speechiness: f64,
    pub acousticness: f64,
    pub instrumentalness: f64,
    pub liveness: f64,
    pub valence: f64,
    pub tempo: f64,
    pub time_signature: usize,
    pub track_genre: String,
}

#[derive(Debug, serde::Deserialize, Clone)]
pub struct Albalone {
    pub sex: char,
    pub length: f64,
    pub diameter: f64,
    pub height: f64,
    pub whole_weight: f64,
    pub shucked_weight: f64,
    pub viscera_weight: f64,
    pub shell_weight: f64,
    pub rings: f64,
}
